package com.blanclabs.position.test.ecorpbankingsolution.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blanclabs.position.test.ecorpbankingsolution.model.PredictionModel;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PredictFakeCardsController {

	@Autowired
	private PredictionModel predictionModel;

	@RequestMapping(value = "/fake-predict/{day}", method = RequestMethod.GET)
	public long predict(@PathVariable("day") int day) {
		log.info("GET request for predict day {}", day);
		long result = predictionModel.predict(day);
		log.info("GET request for predict day {} with result {}", day, result);
		return result;
	}
}
