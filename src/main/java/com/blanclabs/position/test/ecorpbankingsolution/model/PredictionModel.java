package com.blanclabs.position.test.ecorpbankingsolution.model;

import static com.blanclabs.position.test.ecorpbankingsolution.util.MathUtil.factorial;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PredictionModel {

	public long predict(long day) {
		log.info("Predicting day {}", day);
		return day * factorial(day);
	}
}
