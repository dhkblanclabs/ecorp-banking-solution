package com.blanclabs.position.test.ecorpbankingsolution.util;

public class MathUtil {
	public static long factorial(long factor) {
		if (factor <= 1)
			return 1;
		else
			return factor * factorial(factor - 1);
	}
}
