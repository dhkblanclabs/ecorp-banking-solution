package com.blanclabs.position.test.ecorpbankingsolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.blanclabs.position.test.ecorpbankingsolution")
@SpringBootApplication
public class EcorpBankingSolutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcorpBankingSolutionApplication.class, args);
	}
}
