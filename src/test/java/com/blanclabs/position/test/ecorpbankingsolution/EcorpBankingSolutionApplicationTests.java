package com.blanclabs.position.test.ecorpbankingsolution;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
public class EcorpBankingSolutionApplicationTests {

	private static final String USERNAME = "admin";
	private static final String PASSWORD = "admin";
	private static final String BAD_PASSWORD = "password";
	@Autowired
	private MockMvc mvc;

	@Test
	@WithMockUser(username = USERNAME, password = PASSWORD)
	public void predictDaySixTest() throws Exception {
		final long factorialResult = 6 * 5 * 4 * 3 * 2 * 1;
		mvc.perform(get("/fake-predict/6")).andExpect(status().isOk())
				.andExpect(content().string(String.valueOf(factorialResult * 6)));
	}

	@Test
	@WithMockUser(username = USERNAME, password = PASSWORD)
	public void predictDaySixNoMethodTest() throws Exception {
		mvc.perform(post("/fake-predict/6")).andExpect(status().is(403));
	}

	@Test
	public void predictDaySixUnauthorizedTest() throws Exception {
		mvc.perform(get("/fake-predict/6")).andExpect(status().is(401));
	}

}
