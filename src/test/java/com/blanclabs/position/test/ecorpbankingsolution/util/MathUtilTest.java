package com.blanclabs.position.test.ecorpbankingsolution.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class MathUtilTest {

	@Test
	public final void testFactorial() {
		final long factorialNumber = 6 * 5 * 4 * 3 * 2 * 1;
		assertEquals(factorialNumber, MathUtil.factorial(6));
	}

}
